// Fill out your copyright notice in the Description page of Project Settings.

#include "PickupActor.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Assignment3Character.h"

// Sets default values
APickupActor::APickupActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create root component
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	// Create and attach visible component;
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualRepresentation"));
	VisibleComponent->SetupAttachment(RootComponent);

	// Assign a mesh to the pickup.
	static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshAsset(
		TEXT("/Game/StarterContent/Shapes/Shape_Cone.Shape_Cone")
	);
	if (MeshAsset.Succeeded()) VisibleComponent->SetStaticMesh(MeshAsset.Object);

	// Get the base material and apply this to our VisibleComponent.
	// This will be replaced with a dynamic, tinted version later.
	static ConstructorHelpers::FObjectFinder<UMaterial> MatAsset(
		TEXT("/Game/Assignment3/Materials/Unstable.Unstable")
	);
	if (MatAsset.Succeeded()) VisibleComponent->SetMaterial(0, MatAsset.Object);

	// This is how we know if the player has picked up the pickup.
	VisibleComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	VisibleComponent->OnComponentBeginOverlap.AddDynamic(this, &APickupActor::BeginOverlap);

	// Default pickup type.
	PickupEffect = PickupType::HEALTH;
	// Corresponding pickup colour.
	LinearColour = FLinearColor::Red;
}

// Called when the game starts or when spawned
void APickupActor::BeginPlay()
{
	Super::BeginPlay();
	
	// Use dynamic instance of material with tint based on type.
	if (PickupEffect != PickupType::HEALTH) LinearColour = (PickupEffect == PickupType::SPEED ? FLinearColor::Blue : FLinearColor::White);
	TintedVisibleMaterial = VisibleComponent->CreateAndSetMaterialInstanceDynamic(0);
	TintedVisibleMaterial->SetVectorParameterValue("Tint", LinearColour.HSVToLinearRGB());
	TintedVisibleMaterial->SetVectorParameterValue("TintEmissive", LinearColour);
}

// Called every frame
void APickupActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called when an overlap is generated between this actor and some other actor
void APickupActor::BeginOverlap(
	UPrimitiveComponent *OverlappedComp,
	AActor *OtherActor,
	UPrimitiveComponent *OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult &SweepResult
) {
	// If the other actor was the player...
	if (OtherActor->IsA(AAssignment3Character::StaticClass())) {

		AAssignment3Character *PlayerActor = Cast<AAssignment3Character>(OtherActor);

		if (PlayerActor) {
			switch (PickupEffect) {
			case PickupType::HEALTH:
				// Add 5 to player's health.
				PlayerActor->OffsetHealth(5);
				break;

			case PickupType::SPEED:
				// Increase walk speed by 20%.
				PlayerActor->GetCharacterMovement()->MaxWalkSpeed *= 1.2;
				break;

			case PickupType::SHIELD:
				// Activate shield for 10 seconds.
				PlayerActor->ActivateShieldTemporary(10);
				break;
			}

			// Destroy as we have been picked up.
			Destroy();
		}
	}
}