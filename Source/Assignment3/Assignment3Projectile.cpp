// Copyright Epic Games, Inc. All Rights Reserved.

#include "Assignment3Projectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "Components/PointLightComponent.h"
#include "Components/StaticMeshComponent.h"
#include "AgentActor.h"
#include "Assignment3Character.h"
#include "Kismet/GameplayStatics.h"

AAssignment3Projectile::AAssignment3Projectile() 
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionComp"));
	CollisionComp->InitSphereRadius(.1f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &AAssignment3Projectile::OnHit);		// set up a notification for when this component hits something blocking

	// We also don't want the collision component to be visible - our visible sphere will be larger than this collision sphere
	CollisionComp->SetVisibility(false);

	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	//ProjectileMovement->bShouldBounce = true;

	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;

	// Setup point light and attach to root component
	LightComp = CreateDefaultSubobject<UPointLightComponent>(TEXT("PointLightComp"));
	LightComp->SetupAttachment(RootComponent);

	// Hit sound
	static ConstructorHelpers::FObjectFinder<USoundCue> HitSoundAsset(TEXT("SoundCue'/Game/StarterContent/Audio/Steam01_Cue.Steam01_Cue'"));
	if (HitSoundAsset.Succeeded()) HitSound = HitSoundAsset.Object;
}

void AAssignment3Projectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// Play hit sound
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, GetActorLocation());

	// Only add impulse and destroy projectile if we hit a physics
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		if (OtherComp->IsSimulatingPhysics()) {
			OtherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());
			Destroy();
		}

		// Attempt to cast to our agent
		AAgentActor *shootTarget = Cast<AAgentActor>(OtherActor);

		// If successful case then deal damage.
		if (shootTarget) {

			// If this kills the actor, increment the kill count of the shooter.
			if (shootTarget->OffsetHealth(-Shooter->Damage) <= 0) {
				Shooter->KillCount += 1;
				UE_LOG(LogTemp, Warning, TEXT("Shooter kill count: %i!"), Shooter->KillCount);
			}
			// If this does not kill the actor...
			else {
				// Temporarily freeze them according to the player's freeze stat.
				shootTarget->FreezeTemporary(Shooter->FreezeStat);
			}

			// Remove the projectile so it doesn't sit there floating if the actor dies or moves.
			Destroy();
		}
	}
}