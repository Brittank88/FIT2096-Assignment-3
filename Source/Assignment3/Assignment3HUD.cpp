// Copyright Epic Games, Inc. All Rights Reserved.

#include "Assignment3HUD.h"
#include "Engine/Canvas.h"
#include "Engine/Engine.h"
#include "Engine/Texture2D.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Widgets/SWeakWidget.h"
#include "SStartMenuWidget.h"
#include "SPauseMenuWidget.h"
#include "SEndScreenWidget.h"
#include "Assignment3Character.h"
#include "Assignment3GameMode.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundClass.h"

AAssignment3HUD::AAssignment3HUD()
{
	// Set the crosshair texture
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshairTexObj(TEXT("/Game/FirstPerson/Textures/FirstPersonCrosshair"));
	if (CrosshairTexObj.Succeeded()) CrosshairTex = CrosshairTexObj.Object;

	// Get background music audio cue.
	static ConstructorHelpers::FObjectFinder<USoundCue> BGMusicAsset(TEXT("SoundBase'/Game/StarterContent/Audio/Starter_Music_Cue.Starter_Music_Cue'"));
	if (BGMusicAsset.Succeeded()) BGMusic = BGMusicAsset.Object;

	// We initialise with no menu open (at least, until BeginPlay).
	CurrentlyOpen = MenuType::NONE;
}


void AAssignment3HUD::DrawHUD()
{
	Super::DrawHUD();

	// Draw very simple crosshair

	// Find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	// Draw crosshair if not paused.
	if (!UGameplayStatics::IsGamePaused(PlayerOwner->GetWorld())) {
		// Offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
		FCanvasTileItem TileItem(FVector2D(Center.X, Center.Y + 20.0f), CrosshairTex->Resource, FLinearColor::White);
		TileItem.BlendMode = SE_BLEND_Translucent;
		Canvas->DrawItem(TileItem);
	}
}

void AAssignment3HUD::BeginPlay()
{
	Super::BeginPlay();

	/*
	if (GEngine && GEngine->GameViewport) {
		GEngine->GameViewport->AddViewportWidgetContent(
			SAssignNew(PlayerHUDWidget, SWeakWidget)
		);
	}
	*/

	GameModeInstance = PlayerOwner->GetWorld()->GetAuthGameMode<AAssignment3GameMode>();

	StartMenuWidget = SNew(SStartMenuWidget).OwningHUD(this);
	PauseMenuWidget = SNew(SPauseMenuWidget).OwningHUD(this);
	EndScreenWidget = SNew(SEndScreenWidget).OwningHUD(this);

	// Play background music. Will immediately be paused again, but we need this to run only once.
	BGMusicPlayingRef = UGameplayStatics::SpawnSound2D(GetWorld(), BGMusic);
	StartOfGame();

	// Bind start, pause, restart events
	// Ensure that the start and restart actions can execute even when paused, or we will not be able to use those keybinds on pausing
	PlayerOwner->InputComponent->BindAction("StartGame", IE_Pressed, this, &AAssignment3HUD::OnStartGame).bExecuteWhenPaused = true;
	PlayerOwner->InputComponent->BindAction("PauseGame", IE_Pressed, this, &AAssignment3HUD::OnPauseGame);
	PlayerOwner->InputComponent->BindAction("RestartGame", IE_Pressed, this, &AAssignment3HUD::OnRestartGame).bExecuteWhenPaused = true;
}

void AAssignment3HUD::showMenu(MenuType MenuType)
{
	if (GEngine && GEngine->GameViewport && CurrentlyOpen == MenuType::NONE) {

		switch (MenuType) {
			case MenuType::START_MENU:
				GEngine->GameViewport->AddViewportWidgetContent(
					SAssignNew(MenuWidgetContainer, SWeakWidget).PossiblyNullContent(StartMenuWidget.ToSharedRef())
				);
				break;

			case MenuType::PAUSE_MENU:
				GEngine->GameViewport->AddViewportWidgetContent(
					SAssignNew(MenuWidgetContainer, SWeakWidget).PossiblyNullContent(PauseMenuWidget.ToSharedRef())
				);
				break;

			case MenuType::END_SCREEN:
				GEngine->GameViewport->AddViewportWidgetContent(
					SAssignNew(MenuWidgetContainer, SWeakWidget).PossiblyNullContent(StartMenuWidget.ToSharedRef())
				);

				break;
		}

		if (PlayerOwner) {
			PlayerOwner->bShowMouseCursor = true;
			PlayerOwner->SetInputMode(FInputModeUIOnly());
		}

		CurrentlyOpen = MenuType;
	}
}

void AAssignment3HUD::hideMenu()
{
	if (GEngine && GEngine->GameViewport && (StartMenuWidget.IsValid() || PauseMenuWidget.IsValid() || EndScreenWidget.IsValid()) && CurrentlyOpen != MenuType::NONE) {
		GEngine->GameViewport->RemoveViewportWidgetContent(MenuWidgetContainer.ToSharedRef());

		if (PlayerOwner) {
			PlayerOwner->bShowMouseCursor = false;
			PlayerOwner->SetInputMode(FInputModeGameOnly());
		}

		CurrentlyOpen = MenuType::NONE;
	}
}

// Handling event for 'q' keypress
void AAssignment3HUD::OnStartGame() {
	hideMenu();

	UGameplayStatics::SetGamePaused(PlayerOwner->GetWorld(), false);
}

// Handling event for 'p' keypress
void AAssignment3HUD::OnPauseGame() {
	showMenu(MenuType::PAUSE_MENU);

	UGameplayStatics::SetGamePaused(PlayerOwner->GetWorld(), true);
}

// Handling event for 'r' keypress
void AAssignment3HUD::OnRestartGame() {
	GameModeInstance->RestartGame();

	//StartOfGame();
}

void AAssignment3HUD::StartOfGame() {
	if (CurrentlyOpen != MenuType::NONE) hideMenu();
	showMenu(MenuType::START_MENU);

	UGameplayStatics::SetGamePaused(PlayerOwner->GetWorld(), true);
}