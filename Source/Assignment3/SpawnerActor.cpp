// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnerActor.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"
#include "AgentActor.h"

// Sets default values
ASpawnerActor::ASpawnerActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create root component
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	// Create the visual component and attach it to the root component
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualRepresentation"));
	VisibleComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	VisibleComponent->SetHiddenInGame(true);
	VisibleComponent->SetupAttachment(RootComponent);

	// Assign the static mesh to the visible component
	static ConstructorHelpers::FObjectFinder<UStaticMesh> VisibleAsset(
		TEXT("/Game/StarterContent/Shapes/Shape_NarrowCapsule.Shape_NarrowCapsule")
	);
	if (VisibleAsset.Succeeded()) VisibleComponent->SetStaticMesh(VisibleAsset.Object);

	// Assign a 'dev' material. Anything recogniseable will do
	static ConstructorHelpers::FObjectFinder<UMaterial> VisibleMaterial(
		TEXT("/Game/StarterContent/Materials/M_ColorGrid_LowSpec.M_ColorGrid_LowSpec")
	);
	if (VisibleMaterial.Succeeded()) VisibleComponent->SetMaterial(0, VisibleMaterial.Object);

	// Spawn sound
	static ConstructorHelpers::FObjectFinder<USoundCue> SpawnSoundAsset(TEXT("SoundCue'/Game/StarterContent/Audio/Steam01_Cue.Steam01_Cue'"));
	if (SpawnSoundAsset.Succeeded()) SpawnSound = SpawnSoundAsset.Object;

	// Spawn particles
	static ConstructorHelpers::FObjectFinder<UParticleSystem> SpawnParticleAsset(TEXT("ParticleSystem'/Game/StarterContent/Particles/P_Explosion.P_Explosion'"));
	if (SpawnParticleAsset.Succeeded()) SpawnParticle = SpawnParticleAsset.Object;

	// Define default publicly editable spawning values
	SpawnInterval = 3;
	StartTime = 3;
	SpawnCountdown = 0;
	MaxSpawns = 1;

	// Default publicly editable values for spawned agents
	AgentWaypointTrackIndex = 0;
	AgentSize = 1.f;
	AgentBaseSpeed = 2.f;
	AgentBaseHealth = 10;
	AgentBaseDamageMin = 1;
	AgentBaseDamageMax = 3;
	AgentBaseDamageSpeed = .5f;

	AgentAOE = false;
	AgentShooting = false;
	AgentRadius = 300.f;
}

// Called when the game starts or when spawned
void ASpawnerActor::BeginPlay()
{
	Super::BeginPlay();
	
	// Timer for delay until first spawn.
	GetWorldTimerManager().SetTimer(StartTimerHandle, this, &ASpawnerActor::AdvanceTimer, 1.f, true);
}

// Called every frame
void ASpawnerActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// StartSpawning becomes true once the initial delay period has reached completion
	if (bStartSpawning)
	{
		// Decrease the countdown until the next spawn by the time since the last tick
		SpawnCountdown -= DeltaTime;
		// If the countdown hits zero, perform a spawn and reset the countdown
		if (SpawnCountdown <= 0)
		{
			// The transform for spawning - we use this more than once so I have extracted it to a variable
			FTransform SpawnTransform = FTransform(FRotator::ZeroRotator, GetActorLocation());

			// Begin spawning but defer until we're done setting any values we need to
			AActor *tempReference = GetWorld()->SpawnActorDeferred<AActor>(SpawnObject, SpawnTransform);

			// If we're ready we can finalise the spawn
			if (tempReference) {
				// If we are spawning an agent, set the appropriate agent values
				if (AAgentActor *tempReferenceAgent = Cast<AAgentActor>(tempReference)) {
					tempReferenceAgent->WaypointTrack = AgentWaypointTrackIndex;
					tempReferenceAgent->Size = AgentSize;
					tempReferenceAgent->BaseSpeed = AgentBaseSpeed;
					tempReferenceAgent->BaseHealth = AgentBaseHealth;
					tempReferenceAgent->BaseDamageMin = AgentBaseDamageMin;
					tempReferenceAgent->BaseDamageMax = AgentBaseDamageMax;
					tempReferenceAgent->BaseDamageSpeed = AgentBaseDamageSpeed;
					tempReferenceAgent->AOE = AgentAOE;
					tempReferenceAgent->Radius = AgentRadius;
					tempReferenceAgent->Shooting = AgentShooting;
				}

				// Finalise the spawn
				UGameplayStatics::FinishSpawningActor(tempReference, SpawnTransform);

				// Play spawn sound
				UGameplayStatics::PlaySoundAtLocation(GetWorld(), SpawnSound, GetActorLocation());

				// Show spawn particles
				if (SpawnParticle != NULL)
					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), SpawnParticle, GetActorLocation());

				// Tracking how many actors we have spawned thus far
				SpawnCount++;
			}

			// Reset the spawn countdown
			SpawnCountdown = SpawnInterval;
		}
	}

	// Destroy this spawner if it has spawned its maximum amount of actors
	if (SpawnCount >= MaxSpawns) Destroy();
}

void ASpawnerActor::AdvanceTimer()
{
	// Decrease the starting delay value
	--StartTime;
	// When we've waited the full delay duration, clear this timer and enable spawning
	if (StartTime < 1)
	{
		GetWorldTimerManager().ClearTimer(StartTimerHandle);
		bStartSpawning = true;
	}
}