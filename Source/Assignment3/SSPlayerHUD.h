// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widgets/SCompoundWidget.h"
#include "Assignment3HUD.h"
#include "SlateBasics.h"
#include "SlateExtras.h"

/**
 * 
 */
class ASSIGNMENT3_API SSPlayerHUD : public SCompoundWidget
{
public:
	SLATE_BEGIN_ARGS(SSPlayerHUD) {}
	SLATE_ARGUMENT(TWeakObjectPtr<class AAssignment3HUD>, OwningHUD)
	SLATE_END_ARGS()

	/** Constructs this widget with InArgs */
	void Construct(const FArguments& InArgs);

	// HUD that created this widget.
	TWeakObjectPtr<class AAssignment3HUD> OwningHUD;

	virtual bool SupportsKeyboardFocus() const override { return true; };

protected:
	class FString HealthStr;
	class FString TimeStr;
	class FString KillsStr;
};
