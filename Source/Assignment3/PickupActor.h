// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CustomEnums.h"
#include "PickupActor.generated.h"

UCLASS()
class ASSIGNMENT3_API APickupActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickupActor();

	UPROPERTY(EditAnywhere, Category = "Pickup")
		PickupType PickupEffect;

	UPROPERTY(VisibleAnywhere)
		class UStaticMeshComponent *VisibleComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// For overlap with player.
	UFUNCTION()
		void BeginOverlap(
			UPrimitiveComponent *OverlappedComp,
			AActor *OtherActor,
			UPrimitiveComponent *OtherComp,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult &SweepResult
		);

	// Dynamic material of the pickup.
	class UMaterialInstanceDynamic *TintedVisibleMaterial;

	// The colour of the pickup.
	FLinearColor LinearColour;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
