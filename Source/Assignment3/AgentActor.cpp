// Fill out your copyright notice in the Description page of Project Settings.


#include "AgentActor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/SphereComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Assignment3Character.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "EngineUtils.h"

// This is an array of each "Track", which in itself is an array of FVectors representing world coordinates.
TArray<TArray<FVector>> WaypointTracks = {
	{
		FVector(-462.0, 161.0 , 100.0), FVector(-470.0, 379.0 , 100.0), FVector(265.0 , 391.0 , 100.0), FVector(276.0 , 632.0 , 100.0),
		FVector(534.0 , 637.0 , 100.0), FVector(534.0 , 859.0 , 100.0), FVector(-232.0, 868.0 , 100.0), FVector(-222.0, 1108.0, 100.0),
		FVector(35.0  , 1111.0, 100.0), FVector(38.0  , 1348.0, 100.0), FVector(261.0 , 1353.0, 100.0), FVector(266.0 , 1573.0, 100.0),
		FVector(27.0  , 1577.0, 100.0), FVector(31.0  , 1823.0, 100.0), FVector(545.0 , 1816.0, 100.0), FVector(541.0 , 1580.0, 100.0),
		FVector(266.0 , 1573.0, 100.0), FVector(261.0 , 1353.0, 100.0), FVector(508.0 , 1350.0, 100.0), FVector(501.0 , 1115.0, 100.0),
		FVector(-222.0, 1108.0, 100.0), FVector(-232.0, 868.0 , 100.0), FVector(534.0 , 859.0 , 100.0), FVector(534.0 , 637.0 , 100.0),
		FVector(276.0 , 632.0 , 100.0), FVector(265.0 , 391.0 , 100.0), FVector(531.0 , 389.0 , 100.0), FVector(537.0 , 161.0 , 100.0)
	},	// Waypoint track 1 (WaterWallRoom)
	{
		FVector(2368.0, 1503.0, 100.0), FVector(2340.0, 1899.0, 100.0), FVector(2090.0, 2139.0, 100.0), FVector(1338.0, 2193.0, 100.0),
		FVector(933.0 , 1765.0, 100.0), FVector(982.0 , 1180.0, 100.0), FVector(1301.0, 763.0 , 100.0), FVector(2375.0, 801.0 , 100.0)
	},	// Waypoint track 2 (Torus Room)
	{
		FVector(1811.0, -603.0, 100.0), FVector(1810.0, -110.0, 100.0), FVector(1325.0, -103.0, 100.0), FVector(1336.0, -598.0, 100.0)
	},	// Waypoint track 3 (RockRoom)
	{
		FVector(1317.0, -1437.0, 100.0), FVector(1275.0, -2314.0, 100.0), FVector(587.0, -2036.0, 100.0), FVector(572.0, -1445.0, 100.0)
	}	// Waypoint track 4 (PyramidRoom)
};

// Sets default values
AAgentActor::AAgentActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create root component
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	// Create the body visual component and attach it to the root component
	VisibleComponentBody = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BodyVisualRepresentation"));
	VisibleComponentBody->SetupAttachment(RootComponent);

	// Assign the NarrowCapsule static mesh to the body visible component
	static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshAsset(
		TEXT("/Game/StarterContent/Shapes/Shape_NarrowCapsule.Shape_NarrowCapsule")
	);
	if (MeshAsset.Succeeded()) VisibleComponentBody->SetStaticMesh(MeshAsset.Object);

	// Get the base material and apply this to our VisibleComponentBody
	// This will be replaced with a dynamic, tinted version later.
	static ConstructorHelpers::FObjectFinder<UMaterial> MatAsset(
		TEXT("/Game/Assignment3/Materials/Unstable.Unstable")
	);
	if (MatAsset.Succeeded()) VisibleComponentBody->SetMaterial(0, MatAsset.Object);

	// Create the arrow visual component and attach it to the root component
	VisibleComponentArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("DirectionVisualRepresentation"));
	VisibleComponentArrow->SetHiddenInGame(false);
	VisibleComponentArrow->SetupAttachment(RootComponent);

	// Used multiple times.
	CenterRelativeLocation = FVector::ZeroVector;
	CenterRelativeLocation.Z = VisibleComponentBody->GetStaticMesh()->GetBounds().GetBox().GetSize().Z / 2;

	// We want the arrow to protrude from halfway up the actor model.
	FVector ArrowRelativeLocation = VisibleComponentArrow->GetRelativeLocation();
	ArrowRelativeLocation.Z = CenterRelativeLocation.Z;
	VisibleComponentArrow->SetRelativeLocation(ArrowRelativeLocation);

	// Initialise default property values
	Size = 1.f;
	BaseSpeed = 2.f;
	TargetedWaypointIndex = 1;	// Target the second waypoint, we will be spawning at the first waypoint
	bIsTurning = true;			// Immediately rotate to face the second waypoint
	BaseHealth = 10;			// Spawn in with a default of 10 health
	BaseDamageMin = 1;			// Deal 1 damage minimum by default
	BaseDamageMax = 3;			// Deal 3 damage maximum by default
	BaseDamageSpeed = .5f;		// Deal damage every 0.5 seconds by default

	AOE = false;
	Shooting = false;
	Radius = 300.f;				// Radius for AOE or shooting agents. Only used when one or more of these is enabled.

	// Setup death effects.
	static ConstructorHelpers::FObjectFinder<UParticleSystem> ExplosionPSAsset(TEXT("ParticleSystem'/Game/StarterContent/Particles/P_Explosion.P_Explosion'"));
	if (ExplosionPSAsset.Succeeded()) ExplosionPS = ExplosionPSAsset.Object;

	static ConstructorHelpers::FObjectFinder<USoundCue> ExplosionSCAsset(TEXT("SoundBase'/Game/StarterContent/Audio/Explosion_Cue.Explosion_Cue'"));
	if (ExplosionSCAsset.Succeeded()) ExplosionSC = ExplosionSCAsset.Object;

	// Overlap status.
	bOverlappingPlayer = false;

	// Frozen status.
	bIsFrozen = false;
}

// Called when the game starts or when spawned
void AAgentActor::BeginPlay()
{
	Super::BeginPlay();

	// We would like to scale our entire agent according to our size statistic
	RootComponent->SetWorldScale3D(FVector(Size, Size, Size));

	// Calculate our actual movement speed from the base speed modifier and the size
	FinalSpeed       = BaseSpeed     * (1 / Size);
	FinalDamageMin   = BaseDamageMin * Size;
	FinalDamageMax   = BaseDamageMax * Size;
	FinalHealth      = BaseHealth    * Size;
	InitialHealth    = FinalHealth;
	FinalDamageSpeed = BaseDamageSpeed * (1 / Size);
	
	AgentTint = FLinearColor(255.f, 0.f, 0.f, 1.f);					// Red as a default linear colour, boosted to glow very brightly
	AgentTint = AgentTint.LinearRGBToHSV();							// Convert to HSV in preparation for hue adjustment
	AgentTint.R = FMath::GetMappedRangeValueClamped(FVector2D(1.f, 3.f), FVector2D(0.f, 270.f), Size);	// New max is 270 as 360 will just wrap back around to the same colour
	AgentTint = AgentTint.HSVToLinearRGB();							// Convert back to LinearRGB after applying hue adjustment

	// Use dynamic instance of material with tint based on size statistic.
	TintedVisibleMaterial = VisibleComponentBody->CreateAndSetMaterialInstanceDynamic(0);
	TintedVisibleMaterial->SetVectorParameterValue("Tint", AgentTint.HSVToLinearRGB());
	TintedVisibleMaterial->SetVectorParameterValue("TintEmissive", AgentTint);

	// We need to know when we are overlapping our body component with the player
	VisibleComponentBody->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	VisibleComponentBody->OnComponentBeginOverlap.AddDynamic(this, &AAgentActor::BeginOverlap);
	VisibleComponentBody->OnComponentEndOverlap.AddDynamic(this, &AAgentActor::EndOverlap);
}

// Called every frame
void AAgentActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Whilst we're frozen, we can't move or deal AOE damage.
	if (bIsFrozen) return;

	// Draw sphere around agent if they are AOE.
	if (AOE) UKismetSystemLibrary::DrawDebugSphere(GetWorld(), GetActorLocation() + CenterRelativeLocation, Radius, 12, AgentTint);

	// Damage all players within radius.
	for (TActorIterator<AAssignment3Character> PlayerItr(GetWorld()); PlayerItr; ++PlayerItr) {

		float DistanceToPlayer = FVector::Dist(GetActorLocation(), PlayerItr->GetActorLocation());

		if (DistanceToPlayer >= Radius && bOverlappingPlayer) {
			EndOverlap(NULL, *PlayerItr, NULL, NULL);
		}
		else if (DistanceToPlayer < Radius && !bOverlappingPlayer) {
			BeginOverlap(NULL, *PlayerItr, NULL, NULL, NULL, FHitResult(NoInit));
		}
	}

	// Get the current targeted waypoint
	FVector TargetedWaypoint = WaypointTracks[WaypointTrack][TargetedWaypointIndex];

	if (bIsTurning) {

		// Get the current angle and angle we need to rotate towards to be facing this waypoint
		FRotator WaypointLookAtRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), TargetedWaypoint);
		FRotator AgentCurrentRotation = GetActorRotation();

		// Remove any pitch and yaw, as we only want left / right rotations
		WaypointLookAtRotation.Pitch = 0;
		WaypointLookAtRotation.Roll = 0;
		AgentCurrentRotation.Pitch = 0;
		AgentCurrentRotation.Roll = 0;

		// Rotate towards the direction of the current targeted waypoint
		RootComponent->SetWorldRotation(FMath::Lerp(
			AgentCurrentRotation,
			WaypointLookAtRotation,
			DeltaTime * FinalSpeed
		));

		// If we're close enough to the correct angle, target the next waypoint
		bIsTurning = FMath::Abs(AgentCurrentRotation.Yaw - WaypointLookAtRotation.Yaw) >= 2.f;
	}
	else {
		// Move towards the current targeted waypoint
		RootComponent->SetWorldLocation(FMath::Lerp(
			GetActorLocation(), TargetedWaypoint, DeltaTime * FinalSpeed
		));

		// If we're close enough to the current targeted waypoint, target the next waypoint
		if (FVector::Dist(GetActorLocation(), TargetedWaypoint) < 2.f)
		{
			TargetedWaypointIndex++;
			if (TargetedWaypointIndex >= WaypointTracks[WaypointTrack].Num()) TargetedWaypointIndex = 0;

			// Begin turning to next waypoint
			bIsTurning = true;
		}
	}
}

// Called when an overlap is generated between this actor and some other actor
void AAgentActor::BeginOverlap(
	UPrimitiveComponent *OverlappedComp,
	AActor *OtherActor,
	UPrimitiveComponent *OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult &SweepResult
) {
	// If the other actor was the player...
	if (OtherActor->IsA(AAssignment3Character::StaticClass()) && !IsActorBeingDestroyed()) {

		// We are overlapping the player.
		bOverlappingPlayer = true;

		// Cast to the player class
		AAssignment3Character *PlayerActor = Cast<AAssignment3Character>(OtherActor);

		// Subtract health with FinalDamageSpeed frequency.
		// Restarting on death is handled in player class.
		GetWorld()->GetTimerManager().SetTimer(
			DamagePlayerTimer,
			[this, PlayerActor]() { PlayerActor->OffsetHealth(-FMath::RandRange(FinalDamageMin, FinalDamageMax)); },
			FinalDamageSpeed,
			true
		);

		/*
		// Get the player's transform (stored by the player on begin)
		FTransform PlayerInitialLocationAndRotation = PlayerActor->InitialLocationAndRotation;
		// Set the transform to the spawning transform
		PlayerActor->SetActorTransform(PlayerInitialLocationAndRotation);
		// Set the controller rotation to the spawning camera angle
		PlayerActor->GetController()->ClientSetRotation(PlayerInitialLocationAndRotation.Rotator());
		// Zero out the velocity, to avoid moving a bit after respawning (in cases such as being respawned whilst walking)
		PlayerActor->GetMovementComponent()->Velocity = FVector::ZeroVector;
		*/
	}
}

void AAgentActor::EndOverlap(UPrimitiveComponent *OverlappedComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex) {
	// If the other actor was the player...
	if (OtherActor->IsA(AAssignment3Character::StaticClass())) {
		bOverlappingPlayer = false;

		GetWorld()->GetTimerManager().ClearTimer(AAgentActor::DamagePlayerTimer);
	}
}

// Offset health by some amount.
int AAgentActor::OffsetHealth(int offset) {
	FinalHealth += offset;

	// Agent's visible brightness adjusts based on health.
	TintedVisibleMaterial->SetScalarParameterValue("HealthMultiply", float(FinalHealth) / float(InitialHealth));

	// Die if dead, lmao.
	if (FinalHealth <= 0) { 
		if (ExplosionPS != NULL)
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionPS, VisibleComponentBody->GetComponentTransform());
		if (ExplosionSC != NULL)
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), ExplosionSC, VisibleComponentBody->GetComponentLocation());

		GetWorld()->GetTimerManager().ClearTimer(AAgentActor::DamagePlayerTimer);
		Destroy();
	}

	UE_LOG(LogTemp, Warning, TEXT("Agent health: %i!"), FinalHealth);
	
	return FinalHealth;
}

void AAgentActor::FreezeTemporary(float duration) {
	bIsFrozen = true;
	GetWorld()->GetTimerManager().PauseTimer(DamagePlayerTimer);

	GetWorld()->GetTimerManager().SetTimer(
		AAgentActor::UnfreezeTimer,
		[this]() {
			bIsFrozen = false;
			if (DamagePlayerTimer.IsValid() && !IsActorBeingDestroyed()) GetWorld()->GetTimerManager().UnPauseTimer(DamagePlayerTimer);
		},
		duration,
		false
	);
}