// Copyright Epic Games, Inc. All Rights Reserved.

#include "Assignment3GameMode.h"
#include "Assignment3HUD.h"
#include "Assignment3Character.h"
#include "MenuPlayerController.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"

AAssignment3GameMode::AAssignment3GameMode()
	: AGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// Use our custom HUD class.
	HUDClass = AAssignment3HUD::StaticClass();
}