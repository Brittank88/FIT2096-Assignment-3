// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "Sound/SoundCue.h"
#include "Assignment3GameMode.generated.h"

UCLASS(minimalapi)
class AAssignment3GameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AAssignment3GameMode();
};



