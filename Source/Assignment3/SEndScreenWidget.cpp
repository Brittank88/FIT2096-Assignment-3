// Fill out your copyright notice in the Description page of Project Settings.


#include "SEndScreenWidget.h"
#include "Assignment3HUD.h"

#define LOCTEXT_NAMESPACE "EndScreen"

void SEndScreenWidget::Construct(const FArguments &InArgs) {

	// Cache a reference to passed owning HUD.
	OwningHUD = InArgs._OwningHUD;

}

#undef LOCTEXT_NAMESPACE