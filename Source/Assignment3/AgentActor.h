// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/EngineTypes.h"
#include "Sound/SoundCue.h"
#include "AgentActor.generated.h"

class KismetMathLibrary;
class Assignment3Character;
class UCameraComponent;
class UPawnMovementComponent;

UCLASS()
class ASSIGNMENT3_API AAgentActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAgentActor();

	// Agent's visible main body
	UPROPERTY(VisibleAnywhere)
		class UStaticMeshComponent *VisibleComponentBody;

	// Visible arrow component
	UPROPERTY(VisibleAnywhere)
		class UArrowComponent *VisibleComponentArrow;

	// Size of the agent - this also affects their speed
	UPROPERTY(EditAnywhere, Category = "Agent|AgentStatistics", meta = (ClampMin = "1.0", ClampMax = "3.0", UIMin = "1.0", UIMax = "3.0"))
		float Size;

	// Base speed of the agent - this is speed of the actor when size is 1
	UPROPERTY(EditAnywhere, Category = "Agent|AgentStatistics", meta = (ClampMin = "0.0", UIMin = "0.0"))
		float BaseSpeed;

	// Maximum health of the agent - this is health of the actor when size is 1
	UPROPERTY(EditAnywhere, Category = "Agent|AgentStatistics", meta = (ClampMin = "0.0", UIMin = "0.0"))
		int BaseHealth;

	// Minimum base damage of the agent - this is damage the actor deals to the player when size is 1
	UPROPERTY(EditAnywhere, Category = "Agent|AgentStatistics", meta = (ClampMin = "0.0", UIMin = "0.0"))
		int BaseDamageMin;

	// Minimum base damage of the agent - this is damage the actor deals to the player when size is 1
	UPROPERTY(EditAnywhere, Category = "Agent|AgentStatistics", meta = (ClampMin = "0.0", UIMin = "0.0"))
		int BaseDamageMax;

	// Minimum base damage speed of the agent - this is how often the agent damages the actor when size is 1
	UPROPERTY(EditAnywhere, Category = "Agent|AgentStatistics", meta = (ClampMin = "0.0", UIMin = "0.0"))
		float BaseDamageSpeed;

	// The waypoint track the agent will use.
	UPROPERTY(EditAnywhere, Category = "Agent|AgentPathing", meta = (ClampMin = "1", ClampMax = "3", UIMin = "1", UIMax = "3"))
		int WaypointTrack;

	// The types the agent can be.
	UPROPERTY(EditAnywhere, Category = "Agent|AgentType|AOE")
		bool AOE;
	UPROPERTY(EditAnywhere, Category = "Agent|AgentType|Shooting")
		bool Shooting;
	UPROPERTY(EditAnywhere, Category = "Agent|AgentType|Both", meta = (ClampMin = "0.0", UIMin = "0.0"))
		float Radius;

	UPROPERTY(VisibleAnywhere)
		bool bIsFrozen;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// FVector for center of agent.
	FVector CenterRelativeLocation;

	// Agent tint colour.
	FLinearColor AgentTint;

	// Dynamic material of the agent.
	class UMaterialInstanceDynamic *TintedVisibleMaterial;

	// The speed of the agent - this is controlled by the size of the agent
	float FinalSpeed;

	// The index of the currently targeted waypoint vector
	int TargetedWaypointIndex;

	// Controls whether we are stopped and turning, or moving to the next waypoint
	bool bIsTurning;

	// The health of the agent - this is controlled by the size of the agent
	int FinalHealth;

	// The original health of the agent
	int InitialHealth;

	// The minimum damage of the agent - this is controlled by the size of the agent
	int FinalDamageMin;

	// The maximum damage of the agent - this is controlled by the size of the agent
	int FinalDamageMax;

	// The damage speed of the agent - this is controlled by the size of the agent
	float FinalDamageSpeed;

	// Death effects.
	UPROPERTY(VisibleAnywhere)
		class UParticleSystem *ExplosionPS;
	UPROPERTY(VisibleAnywhere)
		class USoundCue *ExplosionSC;

	// Delays for a given period before unfreezing the agent.
	FTimerHandle UnfreezeTimer;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Overlap events
	UFUNCTION()
	void BeginOverlap(
		UPrimitiveComponent *OverlappedComp,
		AActor *OtherActor,
		UPrimitiveComponent *OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult &SweepResult
	);
	UFUNCTION()
	void EndOverlap(UPrimitiveComponent *OverlappedComp, AActor *OtherActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex);

	UPROPERTY(VisibleAnywhere)
	bool bOverlappingPlayer;

	UPROPERTY(VisibleAnywhere)
	FTimerHandle DamagePlayerTimer;

	// Offset health by some value.
	UFUNCTION()
	virtual int OffsetHealth(int offset);

	UFUNCTION()
	virtual void FreezeTemporary(float duration);
};
