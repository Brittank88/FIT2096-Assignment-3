// Fill out your copyright notice in the Description page of Project Settings.


#include "SStartMenuWidget.h"
#include "Assignment3HUD.h"

#define LOCTEXT_NAMESPACE "StartMenu"

void SStartMenuWidget::Construct(const FArguments &InArgs) {

	// Cache a reference to passed owning HUD.
	OwningHUD = InArgs._OwningHUD;

	const FMargin ContentPadding = FMargin(400.f, 300.f);
	const FMargin ButtonPadding = FMargin(10.f);

	FSlateFontInfo ButtonTextStyle = FCoreStyle::Get().GetFontStyle("EmbossedText");
	ButtonTextStyle.Size = 40.f;

	FSlateFontInfo TitleTextStyle = ButtonTextStyle;
	ButtonTextStyle.Size = 60.f;

	const FText TitleText = LOCTEXT("TitleText", "Mason Hunt | 30609941");
	const FText PlayText = LOCTEXT("PlayText", "Play Game");

	ChildSlot[
		SNew(SOverlay)
		+ SOverlay::Slot()
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Fill)
		[
			SNew(SImage)
			.ColorAndOpacity(FColor(0,0,0,200))
		]
		+ SOverlay::Slot()
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Fill)
		.Padding(ContentPadding)
		[
			SNew(SVerticalBox)

			// Title text.
			+ SVerticalBox::Slot()
			[
				SNew(STextBlock)
				.Font(TitleTextStyle)
				.Text(TitleText)
				.Justification(ETextJustify::Center)
			]

			// Play game button.
		+ SVerticalBox::Slot()
			.Padding(ButtonPadding)
			.VAlign(VAlign_Center)
			[
				SNew(SButton)
				.OnClicked(this, &SStartMenuWidget::OnPlayClicked)
				.VAlign(VAlign_Center)
				[
					SNew(STextBlock)
					.Font(ButtonTextStyle)
					.Text(PlayText)
					.Justification(ETextJustify::Center)
				]
			]
		]
	];
}

FReply SStartMenuWidget::OnPlayClicked() const
{
	if (OwningHUD.IsValid()) OwningHUD->OnStartGame();

	return FReply::Handled();
}

#undef LOCTEXT_NAMESPACE