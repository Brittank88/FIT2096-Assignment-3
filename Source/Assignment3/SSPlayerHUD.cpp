// Fill out your copyright notice in the Description page of Project Settings.


#include "SSPlayerHUD.h"
#include "Assignment3HUD.h"

#define LOCTEXT_NAMESPACE "HUD"

void SSPlayerHUD::Construct(const FArguments& InArgs)
{
	// Cache a reference to passed owning HUD.
	OwningHUD = InArgs._OwningHUD;

	FSlateFontInfo HUDTextStyle = FCoreStyle::Get().GetFontStyle("EmbossedText");
	HUDTextStyle.Size = 30.f;

	const FMargin ContentPadding = FMargin(40.f, 30.f);

	ChildSlot[
		SNew(SOverlay)
		+ SOverlay::Slot()
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Fill)
		.Padding(ContentPadding)
		[
			SNew(SVerticalBox)

			// Health text.
			+ SVerticalBox::Slot()
			.HAlign(HAlign_Left)
			.VAlign(VAlign_Bottom)
			[
				SNew(STextBlock)
				.Font(HUDTextStyle)
				.Text_Lambda([this]() { return FText::FromString("Health: " + FString::FromInt(1)); })
				.Justification(ETextJustify::Center)
			]

			// Time text.
			+ SVerticalBox::Slot()
			.HAlign(HAlign_Left)
			.VAlign(VAlign_Bottom)
			[
				SNew(STextBlock)
				.Font(HUDTextStyle)
				.Text_Lambda([this]() { return FText::FromString(TimeStr); })
				.Justification(ETextJustify::Center)
			]

			// Kills text.
			+ SVerticalBox::Slot()
			.HAlign(HAlign_Left)
			.VAlign(VAlign_Bottom)
			[
				SNew(STextBlock)
				.Font(HUDTextStyle)
				.Text_Lambda([this]() { return FText::FromString(KillsStr); })
				.Justification(ETextJustify::Center)
			]
		]
	];
}

#undef LOCTEXT_NAMESPACE