// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SlateBasics.h"
#include "SlateExtras.h"
#include "CustomEnums.h"
#include "Sound/SoundCue.h"
#include "Components/AudioComponent.h"
#include "Assignment3GameMode.h"
#include "Assignment3HUD.generated.h"

class UGameplayStatics;

UCLASS()
class AAssignment3HUD : public AHUD
{
	GENERATED_BODY()

public:
	AAssignment3HUD();

	/** Primary draw call for the HUD */
	UFUNCTION()
	virtual void DrawHUD() override;

	/** Menu show/hide functions */
	UFUNCTION()
	void showMenu(MenuType MenuType);
	UFUNCTION()
	void hideMenu();

	UPROPERTY(VisibleAnywhere)
	MenuType CurrentlyOpen;

	/** Starts the game on keypress. */
	UFUNCTION()
	void OnStartGame();

	/** Pauses the game on keypress. */
	UFUNCTION()
	void OnPauseGame();

	/** Restarts the game on keypress. */
	UFUNCTION()
	void OnRestartGame();

	UFUNCTION()
	virtual void StartOfGame();

	UPROPERTY(EditAnywhere)
	class USoundCue *BGMusic;

	UPROPERTY(EditAnywhere)
	class UAudioComponent *BGMusicPlayingRef;

protected:

	/** Crosshair asset pointer */
	class UTexture2D *CrosshairTex;

	TSharedPtr<class SStartMenuWidget> StartMenuWidget;
	TSharedPtr<class SPauseMenuWidget> PauseMenuWidget;
	TSharedPtr<class SEndScreenWidget> EndScreenWidget;
	//TSharedPtr<class SPlayerHUD> PlayerHUDWidget;
	TSharedPtr<class SWidget> MenuWidgetContainer;

	virtual void BeginPlay() override;

	class AAssignment3GameMode *GameModeInstance;
};

