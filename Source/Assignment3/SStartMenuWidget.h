// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SlateBasics.h"
#include "SlateExtras.h"

/**
 * 
 */
class ASSIGNMENT3_API SStartMenuWidget : public SCompoundWidget
{
public:

	SLATE_BEGIN_ARGS(SStartMenuWidget) {}

	SLATE_ARGUMENT(TWeakObjectPtr<class AAssignment3HUD>, OwningHUD)

	SLATE_END_ARGS()

	// Widget construction function.
	void Construct(const FArguments& InArgs);

	// HUD that created this widget.
	TWeakObjectPtr<class AAssignment3HUD> OwningHUD;

	virtual bool SupportsKeyboardFocus() const override { return true; };

	FReply OnPlayClicked() const;
};
