// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/** Menu Type */
UENUM()
enum class MenuType : uint8 {
	START_MENU	UMETA(DisplayName = "Start Menu"),
	PAUSE_MENU	UMETA(DisplayName = "Pause Menu"),
	END_SCREEN	UMETA(DisplayName = "End Screen"),

	NONE		UMETA(DisplayName = "None")
};

/** Pickup Type */
UENUM()
enum class PickupType : uint8 {
	HEALTH	UMETA(DisplayName = "Health"),
	SPEED	UMETA(DisplayName = "Speed"),
	SHIELD	UMETA(DisplayName = "Shield")
};

class ASSIGNMENT3_API CustomEnums
{
public:
	CustomEnums();
	~CustomEnums();
};
