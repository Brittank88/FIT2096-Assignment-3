// Fill out your copyright notice in the Description page of Project Settings.


#include "MenuPlayerController.h"

AMenuPlayerController::AMenuPlayerController() {}

void AMenuPlayerController::SetupInputComponent() {
	Super::SetupInputComponent();

	if (InputComponent) {
		InputComponent->BindAction("OpenMenu", IE_Pressed, this, &AMenuPlayerController::OpenPauseMenu);
	}
}

void AMenuPlayerController::OpenPauseMenu() {
	if (AAssignment3HUD *Assignment3HUD = Cast<AAssignment3HUD>(GetHUD())) {
		Assignment3HUD->showMenu(MenuType::PAUSE_MENU);
	}
}