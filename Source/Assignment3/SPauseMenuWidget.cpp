// Fill out your copyright notice in the Description page of Project Settings.


#include "SPauseMenuWidget.h"
#include "Assignment3HUD.h"

#define LOCTEXT_NAMESPACE "PauseMenu"

void SPauseMenuWidget::Construct(const FArguments &InArgs) {

	// Cache a reference to passed owning HUD.
	OwningHUD = InArgs._OwningHUD;

	const FMargin ContentPadding = FMargin(400.f, 300.f);
	const FMargin ButtonPadding = FMargin(10.f);
	const FMargin SliderPadding = FMargin(5.f);

	FSlateFontInfo ButtonTextStyle = FCoreStyle::Get().GetFontStyle("EmbossedText");
	ButtonTextStyle.Size = 40.f;

	FSlateFontInfo TitleTextStyle = ButtonTextStyle;
	ButtonTextStyle.Size = 60.f;

	FSlateFontInfo SliderTextStyle = ButtonTextStyle;
	SliderTextStyle.Size = 25.f;

	const FText TitleText = LOCTEXT("TitleText", "Paused");
	const FText ResumeText = LOCTEXT("ResumeText", "Resume");
	const FText ExitText = LOCTEXT("ExitText", "Exit");
	const FText MusicVolumeText = LOCTEXT("MusicVolumeText", "Music Volume");

	ChildSlot[
		SNew(SOverlay)
		+ SOverlay::Slot()
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Fill)
		[
			SNew(SImage)
			.ColorAndOpacity(FColor(0, 0, 0, 200))
		]
		+ SOverlay::Slot()
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Fill)
		.Padding(ContentPadding)
		[
			SNew(SVerticalBox)

			// Title text.
			+ SVerticalBox::Slot()
			[
				SNew(STextBlock)
				.Font(TitleTextStyle)
				.Text(TitleText)
				.Justification(ETextJustify::Center)
			]

			// Resume button.
			+ SVerticalBox::Slot()
			.Padding(ButtonPadding)
			.VAlign(VAlign_Center)
			[
				SNew(SButton)
				.OnClicked(this, &SPauseMenuWidget::OnResumeClicked)
				.VAlign(VAlign_Center)
				[
					SNew(STextBlock)
					.Font(ButtonTextStyle)
					.Text(ResumeText)
					.Justification(ETextJustify::Center)
				]
			]

			// Exit button.
			+ SVerticalBox::Slot()
			.Padding(ButtonPadding)
			.VAlign(VAlign_Center)
			[
				SNew(SButton)
				.OnClicked(this, &SPauseMenuWidget::OnExitClicked)
				.VAlign(VAlign_Center)
				[
					SNew(STextBlock)
					.Font(ButtonTextStyle)
					.Text(ExitText)
					.Justification(ETextJustify::Center)
				]
			]

			// Volume slider.
			+SVerticalBox::Slot()
			.Padding(SliderPadding)
			.VAlign(VAlign_Bottom)
			[
				SNew(STextBlock)
				.Font(SliderTextStyle)
				.Text(MusicVolumeText)
				.Justification(ETextJustify::Center)
			]
			+ SVerticalBox::Slot()
			.Padding(SliderPadding)
			.VAlign(VAlign_Center)
			[
				SNew(SSlider)
				.OnValueChanged_Lambda([this](float Volume) { AdjustVolume(Volume); })
				.MinValue(1.e-7f)
				.MaxValue(1.f)
			]
		]
	];
}

FReply SPauseMenuWidget::OnResumeClicked() const
{
	if (OwningHUD.IsValid()) OwningHUD->OnStartGame();

	return FReply::Handled();
}

FReply SPauseMenuWidget::OnExitClicked() const
{
	if (OwningHUD.IsValid()) OwningHUD->OnRestartGame();

	return FReply::Handled();
}

void SPauseMenuWidget::AdjustVolume(float Volume) {
	UE_LOG(LogTemp, Warning, TEXT("Volume: %f"), Volume);

	OwningHUD->BGMusicPlayingRef->AdjustVolume(0, Volume);

	if (OwningHUD->BGMusicPlayingRef && !OwningHUD->BGMusicPlayingRef->IsActive()) OwningHUD->BGMusicPlayingRef->Play();
}

#undef LOCTEXT_NAMESPACE