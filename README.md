# Intro

This is a continuation of Assignment 2, however, I will not be covering Assignment 2's features in this README as they are already covered [here](https://gitlab.com/Brittank88/FIT2096-Assignment-2).

[Demonstration video here.](https://gitlab.com/Brittank88/FIT2096-Assignment-3/-/blob/main/FIT2096%20Assignment3%2030609941.mp4)
[GitLab repo here.](https://gitlab.com/Brittank88/FIT2096-Assignment-3)

# What Works
- Player is functional.
- Shooting enemies freezes them and deals damage to them.
	- Damaged enemies reflect this in their texture.
- Pickups are functional.
- There are 3 paths patrolled by enemies.
- The radius effect controls the AOE size.
- The AOE repetitively damages the player whilst they are in the AOE sphere of effect.
- Game audio and particles are present for all required effects.
- Start of game and pause menus work.

# What Doesn't Work
- Bullet shooter.
	- At best, I was able to get it to lock onto the player (I've since lost that progress...). Projectile firing was severly broken for reasons I couldn't resolve after multiple hours. Firing would occur on its own.
- End of game menu.
- Player HUD.
	- This refused to cast properly despite being identical to every other WORKING menu widget.

# Extra Features
- Projectiles are luminescent to help you see where you're going.
- Projectiles also stick to surfaces and do not collide with the player, for convenience.
- AOE is visualised with a grid-like sphere.
- AOE is disabled whilst the agent is frozen.
- Shield pickup is visualised with a grid-like sphere.
- There is background music! It has a volume slider in the pause menu.
- Pickups have a dynamic texture that is coloured based on their effect.